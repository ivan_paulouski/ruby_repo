class CreateJoinTableCandidateVacancy < ActiveRecord::Migration
  def change
    create_join_table :candidates, :vacancies do |t|
       t.index [:candidate_id, :vacancy_id]
       t.index [:vacancy_id, :candidate_id]
    end
  end
end
