class CreateCandidates < ActiveRecord::Migration
  def change
    create_table :candidates do |t|
      t.string :full_name
      t.string :address
      t.datetime :birth_date
      t.text :about
      t.text :expectation
      t.string :business_trip
      t.string :military_duty

      t.timestamps
    end
  end
end
