class CreateSkillItems < ActiveRecord::Migration
  def change
    create_table :skill_items do |t|
      t.references :skill, index: true
      t.references :vacancy, index: true
      t.string :actual_level

      t.timestamps
    end
  end
end
