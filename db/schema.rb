# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150728161749) do

  create_table "candidates", force: true do |t|
    t.string   "full_name"
    t.string   "address"
    t.datetime "birth_date"
    t.text     "about"
    t.text     "expectation"
    t.string   "business_trip"
    t.string   "military_duty"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "candidates_vacancies", id: false, force: true do |t|
    t.integer "candidate_id", null: false
    t.integer "vacancy_id",   null: false
  end

  add_index "candidates_vacancies", ["candidate_id", "vacancy_id"], name: "index_candidates_vacancies_on_candidate_id_and_vacancy_id"
  add_index "candidates_vacancies", ["vacancy_id", "candidate_id"], name: "index_candidates_vacancies_on_vacancy_id_and_candidate_id"

  create_table "skill_items", force: true do |t|
    t.integer  "skill_id"
    t.integer  "vacancy_id"
    t.string   "actual_level"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "skill_items", ["skill_id"], name: "index_skill_items_on_skill_id"
  add_index "skill_items", ["vacancy_id"], name: "index_skill_items_on_vacancy_id"

  create_table "skills", force: true do |t|
    t.string   "name"
    t.string   "required_level"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vacancies", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
