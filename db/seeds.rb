# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Skill.delete_all
Skill.create!([
	{id: 1, name: "Java", required_level: "Expert"},
	{id: 2, name: "Ruby", required_level: "Working knowledge"},
	{id: 3, name: "jBehave", required_level: "Extensive knowledge"},
	{id: 4, name: "TDD", required_level: "Working knowledge"},
	{id: 5, name: "BDD", required_level: "Extensive knowledge"},
	{id: 6, name: "Hibernate", required_level: "Expert"},
	{id: 7, name: "Spring", required_level: "Expert"}
])

Vacancy.delete_all
Vacancy.create!([
	{id: 1, title: "QA Engineer", description: "Cute QA required"},
	{id: 2, title: "Java EE Developer", description:"Expert in JAVA EE 7"}
])

SkillItem.delete_all
SkillItem.create!([
	{id: 1, skill_id: 1, vacancy_id: 1},
	{id: 2, skill_id: 3, vacancy_id: 1},
	{id: 3, skill_id: 5, vacancy_id: 1},
	{id: 4, skill_id: 1, vacancy_id: 2},
	{id: 5, skill_id: 6, vacancy_id: 2},
	{id: 6, skill_id: 7, vacancy_id: 2}
])

Candidate.delete_all
Candidate.create!([
	{id: 1, full_name: "Ivan Paulouski", address: "Minsk, Kolasa 3"},
	{id: 2, full_name: "Paul Kuzier", address: "Minsk, Kuncevshchina 666"}
])

f = Candidate.first
f.vacancies << Vacancy.first
