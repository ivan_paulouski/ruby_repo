class Skill < ActiveRecord::Base
	has_many :skill_items, dependent: :delete_all
end
