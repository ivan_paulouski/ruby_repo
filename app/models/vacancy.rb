class Vacancy < ActiveRecord::Base
  has_many :skill_items, dependent: :destroy
  has_and_belongs_to_many :candidates
  before_destroy :delete_skills, prepend: true

  private 
  def delete_skills
  	skill_items = self.skill_items
  	skill_items.each do |item|
  		Skill.find(item.skill_id).destroy
  	end
  end
end
