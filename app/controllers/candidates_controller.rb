class CandidatesController < ApplicationController
  respond_to :html, :js

  def new
  	@candidate = Candidate.new
  	@vacancies = Vacancy.all.map {|v| [v.title, v.id]}
  end

  def create
  	@candidate = Candidate.create(candidate_params)
  	@candidate.vacancies << Vacancy.find(params[:vacancy_id])
    if @candidate.save
      redirect_to candidates_url
    else
      render :new
    end
  end

  def index
  	@candidates = Candidate.all
  end

  def destroy
    @candidate = Candidate.find(params[:id])
    @candidate.destroy
    redirect_to candidates_url
  end

  private 
  def candidate_params
  	params.require(:candidate).permit(:full_name, :address)
  end
end
