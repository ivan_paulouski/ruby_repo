class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_vacancy

  def current_vacancy
  	if !session[:vacancy_id].nil?
  		Vacancy.find(session[:vacancy_id])
  	else
  		Vacancy.new
  	end
  end
end
