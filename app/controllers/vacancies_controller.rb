class VacanciesController < ApplicationController
  respond_to :html, :js

  def index
  	@vacancies = Vacancy.all
  end

  def show
  	@vacancy = Vacancy.find(params[:id])
  	skill_items = @vacancy.skill_items
  	@vacancy_skills = get_vacancy_skills
  end

  def new
  	@vacancy = Vacancy.new
  	@title = "New Vacancy"
  end

  def create
  	@vacancies = Vacancy.all
  	@vacancy = Vacancy.create(vacancy_params)
  end

  def delete
  	@vacancy = Vacancy.find(params[:vacancy_id])
  end

  def destroy
  	@vacancies = Vacancy.all
  	@vacancy = Vacancy.find(params[:id])
  	@vacancy.destroy
  end

  def edit
  	@vacancy = Vacancy.find(params[:id])
    session[:vacancy_id] = @vacancy.id
  	@title = "Editing #{@vacancy.title}"
    @skills = get_vacancy_skills
  end

  def update
  	@vacancies = Vacancy.all
  	@vacancy = Vacancy.find(params[:id])
    @skills = get_vacancy_skills
  end

  private
  def vacancy_params
  	params.require(:vacancy).permit(:title, :description)
  end

  def get_vacancy_skills
  	skill_items = @vacancy.skill_items
  	vacancy_skills = []
  	skill_items.each do |item|
  		vacancy_skills << Skill.find(item.skill_id)
  	end
  	vacancy_skills
  end
end
