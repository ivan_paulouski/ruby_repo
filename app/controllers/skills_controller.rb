class SkillsController < ApplicationController
  respond_to :html, :js
  before_action :add_levels, only: [:create, :update, :new, :edit]

  def index
    @skills = get_vacancy_skills
  end

  def show
  	@skill = Skill.find(params[:id])
  end

  def create
  	@skill = Skill.create(skill_params)
    vacancy_id = session[:vacancy_id]
    @skill_item = SkillItem.create!(skill_id: @skill.id, vacancy_id: vacancy_id)
    @skills = get_vacancy_skills
  end

  def new
  	@skill = Skill.new
    @skill_item = SkillItem.new
  	@title = "New skill"
  end

  def edit
  	@skill = Skill.find(params[:id])
  	@title = "Editing #{@skill.name}"
    @skills = get_vacancy_skills
  end

  def update
  	@skill = Skill.find(params[:id])
  	@skill.update_attributes(skill_params)
    @skills = get_vacancy_skills
  end

  def delete
  	@skill = Skill.find(params[:skill_id])
  end

  def destroy
  	@skills = get_vacancy_skills
  	@skill = Skill.find(params[:id])
  	@skill.destroy
  end

  private 
  	def skill_params
  		params.require(:skill).permit(:name, :required_level, :actual_level)
  	end

  	def add_levels
  		@levels = ['Limited knowledge', 'Working knowledge', 'Extensive knowledge', 'Expert']
  	end

    def get_vacancy_skills
    if !session[:vacancy_id].nil?  
      skill_items = Vacancy.find(session[:vacancy_id]).skill_items
      vacancy_skills = []
      skill_items.each do |item|
        vacancy_skills << Skill.find(item.skill_id)
      end
    else
      vacancy_skills = Skill.all
    end
    vacancy_skills
  end
end
