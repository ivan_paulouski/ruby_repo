class SkillItemsController < ApplicationController
  def create
  end

  def update
  end

  def destroy
  end

  private 

  def skill_items_params
  	params.require(:skill_item).permit(:vacancy_id, :skill_id, :actual_level)
  end
end
